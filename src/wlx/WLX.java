/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wlx;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.security.auth.login.FailedLoginException;
import javax.security.auth.login.LoginException;
import org.wikipedia.BaseBot;

/**
 *
 * @author Base <base-w at yandex.ru>
 */
public class WLX {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, FailedLoginException, LoginException {

        BaseBot c = new BaseBot("commons.wikimedia.org");
        c.login(args[0], args[1]);
        BaseBot w = new BaseBot("uk.wikipedia.org");
        w.login(args[0], args[1]);

        String WLX = "";
        String ВЛЩо = "";
        String listSplit = "";
        String Template = "";
        String categoryRegex = "";
        String toolURI = "";
        String  WLname ="";
        switch (args[2]) {
            case "WLM":
                WLX = "WLM";
                ВЛЩо = "ВЛП";
                listSplit = "\\{\\{\\s*[Вв]ЛП-рядок";
                Template = "Template:Monument Ukraine";
                categoryRegex = "(?s).*\\{\\{\\s*[Mm]onument[ _]Ukraine\\s*\\|(?:1=|)\\s*([^}]*)\\s*}}.*";
                toolURI = "http://tools.wmflabs.org/heritage/api/api.php"
                        + "?action=search&format=html&srcountry=ua&srlang=uk"
                        + "&props=image%7Cname%7Caddress%7Cmunicipality%7Clat%7Clon%7Cid%7Ccountry%7Csource%7Cmonument_article%7Cregistrant_url"
                        + "&srid=";
                WLname = "Monuments";
                break;
            case "WLE":
                WLX = "WLE";
                ВЛЩо = "ВЛЗ";
                listSplit = "\\{\\{\\s*[Вв]ЛЗ-рядок";
                Template = "Template:UkrainianNaturalHeritageSite";
                categoryRegex = "(?s).*\\{\\{\\s*[Uu]krainianNaturalHeritageSite\\s*\\|(?:1=|)\\s*([^}]*)\\s*}}.*";
                toolURI = "http://tools.wmflabs.org/wle/?id=";
                WLname = "Earth";
                break;
            default:
                System.out.println("Some shit instead of a known WLX in the 3rd argument");
                System.exit(222);
        }

        Map<String, String> localGalleries = new HashMap<>();
        Map<String, String> commonsGalleries = new HashMap<>();
        Set<String> commonsParsingError = new HashSet<>();

        String[] lists = w.whatTranscludesHere("Шаблон:" + ВЛЩо + "-рядок", 4);
        int grandIterator = 0;
        for (String list : lists) {
            String listText = w.getPageText(list);
            String[] objects = listText.split(listSplit);
            objectsIterator:
            for (int i = 1; i < objects.length; i++) {//start from 1 as the 0th element is what's above the very first object.
                String object = objects[i];
                grandIterator++;
                //61-242-5004
                String IDRegex = "(?s).*\\|\\s*ID\\s*=\\s*([0-9]{2}-[0-9]{3}-[0-9]{4}).*";
                String ID = "";
                if (object.matches(IDRegex)) {
                    ID = object.replaceAll(IDRegex, "$1").trim();
                } else {
                    System.out.println(grandIterator + "\tID does not match for\n" + object);
                    continue objectsIterator;
                }
                String GalleryRegex = "(?s).*\\|\\s*галерея\\s*=\\s*([^\n\\}\\|]*).*";
                String gallery = "";
                if (object.matches(GalleryRegex)) {
                    gallery = object.replaceAll(GalleryRegex, "$1").trim();
                    if (gallery.equals("")) {
                        //System.out.println(grandIterator + "\tGallery is empty");
                        continue objectsIterator;
                    }
                } else {
                    System.out.println(grandIterator + "\tGallery does not match for\n" + object);
                    continue objectsIterator;
                }
//                System.out.println(list + "\t" + ID);
//                System.out.println(list + "\t" + gallery);
                localGalleries.put(ID, gallery);
            }
        }
        String[] categories = c.whatTranscludesHere(Template, 14);
        for (String category : categories) {
            String categoryText = c.getPageText(category);
            category = category.substring(category.indexOf(":") + 1);

            if (categoryText.matches(categoryRegex)) {
                String categoryID = categoryText.replaceAll(categoryRegex, "$1").trim();
                //System.out.println(categoryID + "\t" + category);
                commonsGalleries.put(categoryID, category);
            } else {
                System.out.println("Category does not match for\n" + categoryText);
                commonsParsingError.add(category);
            }
        }
        Set<String> commonsIDs = commonsGalleries.keySet();
        String[] IDs = commonsIDs.toArray(new String[commonsIDs.size()]);
        String WPSucks = "";
        String Different = "";
        for (String ID : IDs) {
            String commonsGallery = commonsGalleries.get(ID);
            if (localGalleries.containsKey(ID)) {
                String localGallery = localGalleries.get(ID);
                if (commonsGallery.equals(localGallery)) {
                    // yay
                } else {
                    Different += "|-\n"
                            + "|" + ID
                            + "||[[:Category:" + commonsGallery + "|" + commonsGallery + "]]<br />({{PAGESINCATEGORY:" + commonsGallery + "|files}})"
                            + "||[[:Category:" + localGallery + "|" + localGallery + "]]<br />({{PAGESINCATEGORY:" + localGallery + "|files}})\n";
                }
            } else {
                WPSucks += "# " + ID + ":\t" + commonsGallery + "\n";
            }
        }

        System.out.println("WPSucks:\n" + WPSucks);
        System.out.println("Different:\n" + Different);

        String CommonsSucks = "";
        Map<String, String> CommonsSucksMap = new HashMap<>();
        //Different = "";
        Set<String> localIDs = localGalleries.keySet();
        IDs = localIDs.toArray(new String[localIDs.size()]);
        for (String ID : IDs) {
            String localGallery = localGalleries.get(ID);
            if (commonsGalleries.containsKey(ID)) {
                String commonsGallery = commonsGalleries.get(ID);
                if (localGallery.equals(commonsGallery)) {
                    //yay
                } else {
                    //those are the same as considered above
                    //Different += "# " + ID + ": [[:Category:" + localGallery + "|"+localGallery+"]]\t" + commonsGallery + "\n";
                }
            } else {
                CommonsSucks += "# " + ID + ":\t" + localGallery + "\n";
                CommonsSucksMap.put(ID, localGallery);
            }
        }

        System.out.println("CommonsSucks:\n" + CommonsSucks);
        System.out.println("Different:\n" + Different);
        c.edit("user:BaseBot/" + WLX + "/UA/Galleries conflict", "{| class=\"wikitable sortable\"\n! ID !! Commons !! Local\n" + Different + "|}\n\n[[Category:Wiki Loves "+WLname+" in Ukraine]]", "creating/updating");
        Collection<String> values = CommonsSucksMap.values();
        Map<String, Integer> valueOccurence = new HashMap<>();
        for (String value : values) {
            int occurence = 0;
            for (String suck : CommonsSucksMap.keySet()) {
                if (CommonsSucksMap.get(suck).equals(value)) {
                    occurence++;
                }
            }
            valueOccurence.put(value, occurence);
        }

        String poligamicalGalleries = "";
        for (String id : CommonsSucksMap.keySet()) {
            String value = CommonsSucksMap.get(id);
            if (valueOccurence.get(value) > 1) {
                poligamicalGalleries += "|-\n|"
                        + "[" + toolURI + id + " " + id + "]"
                        + "||[[:Category:" + value + "|" + value + "]]<br />({{PAGESINCATEGORY:" + value + "|files}})\n";
            }
        }

        c.edit("user:BaseBot/" + WLX + "/UA/Galleries targeted by several ids",
                "{| class=\"wikitable sortable plainlinks\"\n! ID !! Category<br />(from WP)\n" + poligamicalGalleries + "|}\n\n[[Category:Wiki Loves "+WLname+" in Ukraine]]",
                "creating/updating");
    }

}
