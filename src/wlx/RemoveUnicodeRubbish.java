/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wlx;

import java.io.IOException;
import javax.security.auth.login.FailedLoginException;
import javax.security.auth.login.LoginException;
import org.wikipedia.BaseBot;

/**
 *
 * @author Base <base-w at yandex.ru>
 */
public class RemoveUnicodeRubbish {

    public static void main(String[] args) throws IOException, FailedLoginException, LoginException {

        BaseBot w = new BaseBot("uk.wikipedia.org");
        w.login(args[0], args[1]);

        String[] lists = w.whatTranscludesHere("Шаблон:ВЛП-рядок", 4);
        int grandIterator = 0;
        for (String list : lists) {
            String listText = w.getPageText(list);
            String comparison = listText;
            listText = listText.replaceAll("[\\p{Cf}\\p{Co}\\p{Cn}]", "");
            if (!listText.equals(comparison)) {
                w.edit(list, listText, "Чистка керуючих та інших непотрібних символів Юнікоду");
            }
        }
        java.lang.Runtime.getRuntime().exec("shutdown -s");
        System.exit(5455);
    }
}
